﻿// DO NOT TOUCH THIS
void Iterate(IEnumerable<Warrior> allWarriors)
{
    foreach (var warrior in allWarriors)
    {
        Console.WriteLine($"A warrior {warrior.Name} shows up");
        warrior.Attack();
        Console.WriteLine();
    }
}
// END OF DO NOT TOUCH THIS

var ninja = new Warrior("Ninja");
var boxer = new Warrior("Boxer");
var zombie = new Warrior("Zombie");
var karateka = new Warrior("Karateka");

var warriors = new List<Warrior> { ninja, boxer, zombie, karateka };

Iterate(warriors);

class Warrior
{
    public string Name { get; }

    public Warrior(string name)
    {
        Name = name;
    }

    public void Attack()
    {
        Console.WriteLine("I attack as a warrior");
    }
}
